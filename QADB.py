import sqlite3

class QADB(object):
    """Database API for accessing the Question-Answer database"""

    def __init__(self):
        pass

    def open(self, file):
        self.connection = sqlite3.connect(file)
        self.cursor = self.connection.cursor()

    def createSchema(self):
        # Create questions table
        self.cursor.execute('''CREATE TABLE `questions` (`QuestionId` INTEGER, `Question` TEXT UNIQUE, `AnswerId` INTEGER, PRIMARY KEY(QuestionId));''')

        # Create answers table
        self.cursor.execute('''CREATE TABLE `answers` (`AnswerId` INTEGER, `Answer` TEXT UNIQUE, PRIMARY KEY(AnswerId));''')

    def saveOrSelectAnswer(self, answer):
        t = (answer,)
        self.cursor.execute("INSERT OR IGNORE INTO answers (Answer) VALUES (?);", t)
        self.cursor.execute("SELECT AnswerId FROM answers WHERE Answer=?;", t)
        return self.cursor.fetchone()[0]

    def saveQuestion(self, question, answerId):
        t = (question, answerId)
        self.cursor.execute("INSERT OR IGNORE INTO questions (Question, AnswerId) VALUES(?, ?);", t)
        self.cursor.execute("SELECT QuestionId FROM questions WHERE Question = ? AND AnswerID = ?;", t)
        return self.cursor.fetchone()[0]

    def getStoredAnswer(self, question):
        t = (question,)
        self.cursor.execute("SELECT Answer FROM answers JOIN questions ON questions.AnswerId = answers. AnswerId WHERE questions.Question = ?;", t)
        results = self.cursor.fetchone()
        if (results != None):
            return results[0]
        else:
            return None;

    def getAllStoredQuestions(self):
        self.cursor.execute("SELECT question FROM questions")
        results = self.cursor.fetchall()
        if (results != None):
            for result in results:
                yield result[0]
        else:
            yield None

    def commit(self):
        self.connection.commit()


    def close(self):
        self.connection.close()