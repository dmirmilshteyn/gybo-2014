import pyttsx
import speech_recognition as sr

engine = pyttsx.init()

def echo():
    r = sr.Recognizer()
    r.energy_threshold = 5000
    with sr.Microphone() as source:
        audio = r.listen(source)
        text = r.recognize(audio)
        print "You said: " + text
        engine.say("You said: " + text)
        engine.runAndWait()
        
