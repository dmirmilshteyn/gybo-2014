# NOTE: this requires PyAudio because it uses the Microphone class
import speech_recognition as sr
import io, os, subprocess, wave
import math, audioop, collections
import serial

def DetermineEnergyLevelBaseline(source, pause_threshold, quiet_duration, timeout):
    # record audio data as raw samples
    seconds_per_buffer = (source.CHUNK + 0.0) / source.RATE
    pause_buffer_count = int(math.ceil(pause_threshold / seconds_per_buffer)) # number of buffers of quiet audio before the phrase is complete
    quiet_buffer_count = int(math.ceil(quiet_duration / seconds_per_buffer)) # maximum number of buffers of quiet audio to retain before and after
    elapsed_time = 0

    energyTotal = 0
    energyCounts = 0

    # store audio input until the phrase starts
    while True:
        elapsed_time += seconds_per_buffer
        if timeout and elapsed_time > timeout: # handle timeout if specified
            break;

        buffer = source.stream.read(source.CHUNK)
        if len(buffer) == 0: 
            break # reached end of the stream

        # check if the audio input has stopped being quiet
        energy = audioop.rms(buffer, source.SAMPLE_WIDTH) # energy of the audio signal
        energyTotal += energy
        energyCounts += 1
    return energyTotal / energyCounts


r = sr.Recognizer()
with sr.Microphone() as source:                # use the default microphone as the audio source
    print("Measuring background noise...")
    baseline = DetermineEnergyLevelBaseline(source, 0.8, 0.5, 5)
    r.energy_threshold = baseline * 3
    print("Recording voice... Say something!")
    audio = r.record(source, 5)                   # listen for the first phrase and extract it into audio data
    print("Recording complete, recognizing...")

try:
    saidText = str(r.recognize(audio))
    print("You said " + saidText)
    print("Sending by bluetooth!!")
    ser = serial.Serial("COM18", 115200)
    ser.write(saidText)
    ser.write(";")
    ser.close()
    print("All done")
except LookupError:                            # speech is unintelligible
    print("Could not understand audio")