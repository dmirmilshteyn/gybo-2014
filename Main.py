#!/usr/bin/python

# Source: Skype4Py documentation, https://github.com/awahlig/skype4py/blob/master/examples/record.py

import Skype4Py
import sys
import time

import pyttsx
import speech_recognition as sr

import math
import audioop

import urllib
import threading

import serial

import wave, collections

import sqlite3

blue= True

# This variable will get its actual value in OnCall handler
CallStatus = 0

# Here we define a set of call statuses that indicate a call has been either aborted or finished
CallIsFinished = set ([Skype4Py.clsFailed, Skype4Py.clsFinished, Skype4Py.clsMissed, Skype4Py.clsRefused, Skype4Py.clsBusy, Skype4Py.clsCancelled]);

OutFile = 'C:\\Users\\Sandro\\Documents\\UofT\\2014-2015\\GYBO\\code\\gybo-2014\\recording.wav'
Handle = 'danielb.apple'

lk = threading.Lock()

class QADB(object):
    """Database API for accessing the Question-Answer database"""

    def __init__(self):
        pass

    def open(self, file):
        self.connection = sqlite3.connect(file)
        self.cursor = self.connection.cursor()

    def createSchema(self):
        # Create questions table
        self.cursor.execute('''CREATE TABLE `questions` (`QuestionId` INTEGER, `Question` TEXT UNIQUE, `AnswerId` INTEGER, PRIMARY KEY(QuestionId));''')

        # Create answers table
        self.cursor.execute('''CREATE TABLE `answers` (`AnswerId` INTEGER, `Answer` TEXT UNIQUE, PRIMARY KEY(AnswerId));''')

    def saveOrSelectAnswer(self, answer):
        t = (answer,)
        self.cursor.execute("INSERT OR IGNORE INTO answers (Answer) VALUES (?);", t)
        self.cursor.execute("SELECT AnswerId FROM answers WHERE Answer=?;", t)
        return self.cursor.fetchone()[0]

    def saveQuestion(self, question, answerId):
        t = (question, answerId)
        self.cursor.execute("INSERT OR IGNORE INTO questions (Question, AnswerId) VALUES(?, ?);", t)
        self.cursor.execute("SELECT QuestionId FROM questions WHERE Question = ? AND AnswerID = ?;", t)
        return self.cursor.fetchone()[0]

    def getStoredAnswer(self, question):
        t = (question,)
        self.cursor.execute("SELECT Answer FROM answers JOIN questions ON questions.AnswerId = answers. AnswerId WHERE questions.Question = ?;", t)
        results = self.cursor.fetchone()
        if (results != None):
            return results[0]
        else:
            return None;

    def getAllStoredQuestions(self):
        self.cursor.execute("SELECT question FROM questions")
        results = self.cursor.fetchall()
        if (results != None):
            for result in results:
                yield result[0]
        else:
            yield None

    def commit(self):
        self.connection.commit()


    def close(self):
        self.connection.close()

def MakeDB():
    DB = QADB()
    DB.open("qadbtest.db")
    #DB.createSchema()
    return DB

def PutThingsInDB(DB):
    FAQ = [("where are my granchildren", "Your grandchildren live with your son James. They love visiting you. They come over quite often"),
     ("why is the sky blue", "basically because of light scattering"),
     ("where am i", "you are in a house")]
    for (q, a) in FAQ:
        aid = DB.saveOrSelectAnswer(a)
        DB.saveQuestion(q, aid)
    DB.commit()

def SendOverBluetooth(msg):
    global ser
    global blue
    if blue:
        ser.write(msg)
        ser.write(";")
        time.sleep(0.05)
        ser.write(msg)
        ser.write(";")
        time.sleep(0.05)
        ser.write(msg)
        ser.write(";")
        time.sleep(0.05)

def ListenUntilFlag(r, source, flag):
    assert isinstance(source, sr.AudioSource) and source.stream

    # record audio data as raw samples
    frames = collections.deque()
    assert r.pause_threshold >= r.quiet_duration >= 0
    seconds_per_buffer = (source.CHUNK + 0.0) / source.RATE
    elapsed_time = 0

    # store audio input until the phrase starts
    while True:
        elapsed_time += seconds_per_buffer
        if flag():
            break

        buffer = source.stream.read(source.CHUNK)
        if len(buffer) == 0: break # reached end of the stream
        frames.append(buffer)
    
    return (frames, seconds_per_buffer)

def TranscribeListenAtTimes(r, source, times):
    #print "This was actually called!"
    
    assert isinstance(source, sr.AudioSource) and source.stream

    # record audio data as raw samples
    frames = collections.deque()
    assert r.pause_threshold >= r.quiet_duration >= 0
    seconds_per_buffer = (source.CHUNK + 0.0) / source.RATE
    elapsed_time = 0

    all_data = []

    done = False
    i = 0
    while not done:
        if (i >= len(times)):
            start = 1000000000
            end = 1000000000
        else:
            start = times[i][0]
            end = times[i][1]
        
        # read audio input until start
        while not done:
            elapsedTime += seconds_per_buffer
            buffer = source.stream.read(source.CHUNK)
            if len(buffer) == 0:
                done = True
                break # reached end of the stream
            frames.append(buffer)

            if elapsedTime >= start:
                break

        # ignore audio until end
        while not done:
            elapsed_time += seconds_per_buffer
            buffer = source.stream.read(source.CHUNK)
            if len(buffer) == 0:
                done = True
                break # reached end of the stream

            # check if the audio input has stopped being quiet
            energy = audioop.rms(buffer, source.SAMPLE_WIDTH) # energy of the audio signal
            if energy > r.energy_threshold:
                break

            if len(frames) > quiet_buffer_count: # ensure we only keep the needed amount of quiet buffers
                frames.popleft()



     # obtain frame data
    for i in range(quiet_buffer_count, pause_count): frames.pop() # remove extra quiet frames at the end
    frame_data = b"".join(list(frames))
    
    return (sr.AudioData(source.RATE, r.samples_to_flac(source, frame_data)),
            startTime,
            elapsedTime)

def TranscribeListen(r, source):
    #print "This was actually called!"
    
    assert isinstance(source, sr.AudioSource) and source.stream

    # record audio data as raw samples
    frames = collections.deque()
    assert r.pause_threshold >= r.quiet_duration >= 0
    seconds_per_buffer = (source.CHUNK + 0.0) / source.RATE
    pause_buffer_count = int(math.ceil(r.pause_threshold / seconds_per_buffer)) # number of buffers of quiet audio before the phrase is complete
    quiet_buffer_count = int(math.ceil(r.quiet_duration / seconds_per_buffer)) # maximum number of buffers of quiet audio to retain before and after
    elapsed_time = 0

    # store audio input until the phrase starts
    while True:
        elapsed_time += seconds_per_buffer
        if timeout and elapsed_time > timeout: # handle timeout if specified
            raise TimeoutError("listening timed out")

        buffer = source.stream.read(source.CHUNK)
        if len(buffer) == 0: break # reached end of the stream
        frames.append(buffer)

        # check if the audio input has stopped being quiet
        energy = audioop.rms(buffer, source.SAMPLE_WIDTH) # energy of the audio signal
        if energy > r.energy_threshold:
            break

        if len(frames) > quiet_buffer_count: # ensure we only keep the needed amount of quiet buffers
            frames.popleft()

    startTime = elapsedTime

    # read audio input until the phrase ends
    pause_count = 0
    while True:
        elapsedTime += seconds_per_buffer
        buffer = source.stream.read(source.CHUNK)
        if len(buffer) == 0: break # reached end of the stream
        frames.append(buffer)

        # check if the audio input has gone quiet for longer than the pause threshold
        energy = audioop.rms(buffer, source.SAMPLE_WIDTH) # energy of the audio signal
        if energy > r.energy_threshold:
            pause_count = 0
        else:
            pause_count += 1
        if pause_count > pause_buffer_count: # end of the phrase
            break

    endTime = elapsedTime

     # obtain frame data
    for i in range(quiet_buffer_count, pause_count): frames.pop() # remove extra quiet frames at the end
    frame_data = b"".join(list(frames))
    
    return (sr.AudioData(source.RATE, r.samples_to_flac(source, frame_data)),
            startTime,
            elapsedTime)

def MyListen(r, source, timeout = None):
    #print "This was actually called!"
    
    assert isinstance(source, sr.AudioSource) and source.stream

    # record audio data as raw samples
    frames = collections.deque()
    assert r.pause_threshold >= r.quiet_duration >= 0
    seconds_per_buffer = (source.CHUNK + 0.0) / source.RATE
    pause_buffer_count = int(math.ceil(r.pause_threshold / seconds_per_buffer)) # number of buffers of quiet audio before the phrase is complete
    quiet_buffer_count = int(math.ceil(r.quiet_duration / seconds_per_buffer)) # maximum number of buffers of quiet audio to retain before and after
    elapsed_time = 0

    # store audio input until the phrase starts
    while True:
        elapsed_time += seconds_per_buffer
        if timeout and elapsed_time > timeout: # handle timeout if specified
            raise TimeoutError("listening timed out")

        buffer = source.stream.read(source.CHUNK)
        if len(buffer) == 0: break # reached end of the stream
        frames.append(buffer)

        # check if the audio input has stopped being quiet
        energy = audioop.rms(buffer, source.SAMPLE_WIDTH) # energy of the audio signal
        if energy > r.energy_threshold:
            break

        if len(frames) > quiet_buffer_count: # ensure we only keep the needed amount of quiet buffers
            frames.popleft()

    print "Start recording!"
    SendOverBluetooth("L 245;");
    SendOverBluetooth("R 245;");

    # read audio input until the phrase ends
    pause_count = 0
    while True:
        buffer = source.stream.read(source.CHUNK)
        if len(buffer) == 0: break # reached end of the stream
        frames.append(buffer)

        # check if the audio input has gone quiet for longer than the pause threshold
        energy = audioop.rms(buffer, source.SAMPLE_WIDTH) # energy of the audio signal
        if energy > r.energy_threshold:
            pause_count = 0
        else:
            pause_count += 1
        if pause_count > pause_buffer_count: # end of the phrase
            break

    print "End recording!"

     # obtain frame data
    for i in range(quiet_buffer_count, pause_count): frames.pop() # remove extra quiet frames at the end
    frame_data = b"".join(list(frames))
    
    return sr.AudioData(source.RATE, r.samples_to_flac(source, frame_data))

def InfiniteLoop(SpeechToText, TtsEngine, DB):
    while True:
        t = GetTextLoop(SpeechToText, TtsEngine)
        print "You said: " + t
        #TtsEngine.say('You said: ' + t)

        if not StringIsQuestion(t):
            print "That's not a question..."
            continue

        a = GetAnswer(DB, t)

        if a:
            #TtsEngine.say(a)
            #TtsEngine.runAndWait()
            print "Answer: " + a
        else:        
            #TtsEngine.say('I don\'t know the answer to your question. Hold on while I call for help.')
            #TtsEngine.runAndWait()
            print "I don't know the answer"

def ShouldCallForHelp(SpeechToText, TtsEngine):
    TtsEngine.say('I don\'t know the answer to your question. Would you like me to call someone to answer your question?')
    TtsEngine.runAndWait()
    t = GetTextLoop(SpeechToText, TtsEngine, quiet=False)
    words = [w.lower() for w in t.split()]
    
    if "yes" in words or "sure" in words or "okay" in words or "ok" in words:
        TtsEngine.say("Okay, starting call now")
        TtsEngine.runAndWait()
        return True

    print "Not placing call!"

    return False

def AnswerQuestions(SpeechToText, TtsEngine, DB):
    while True:
        t = GetTextLoop(SpeechToText, TtsEngine)
        print "You said: " + t
        #TtsEngine.say('You said: ' + t)

        if not StringIsQuestion(t):
            print "That's not a question..."
            continue

        a = GetAnswer(DB, t)

        if a:
            print "Answer: " + a
            TtsEngine.say(a)
            TtsEngine.runAndWait()
        else:
            #TtsEngine.say('I don\'t know the answer to your question. Hold on while I call for help.')
            #TtsEngine.runAndWait()
            print "I don't know the answer"
            if ShouldCallForHelp(SpeechToText, TtsEngine):
                return

def GetAnswer(db, question):
    d = {}
    ts = []

    answer = db.getStoredAnswer(question)
    if answer:
        return answer

    for q in db.getAllStoredQuestions():
        t = SimilarityThread(question, q, d)
        ts.append(t)
        t.start()
        
    for t in ts:
        t.join()
        
    scored = []
    threshold = 0.85
    
    for ((_, q), s) in d.iteritems():
        if s >= threshold:
            scored.append((s, q))

    scored.sort(key=lambda (s, q): s)

    if not scored:
        return False

    best = scored[-1][1]

    return db.getStoredAnswer(best)
        
def GetSimilarityUrl(s1, s2):
    baseUrl = "http://swoogle.umbc.edu/StsService/GetStsSim"
    query = urllib.urlencode({"operation":"api", "phrase1":s1, "phrase2":s2})
    return baseUrl + "?" + query

def foo():
    rand = ["asdf", "awefawef", "awefawefg", "gegserg"]
    d = {}
    ts = []
    for (s1, s2) in [(x, y) for x in rand for y in rand]:
        t = SimilarityThread(s1, s2, d)
        ts.append(t)
        t.start()
    for t in ts:
        t.join()
    return d

def Similarity(s1, s2):
    while True:
        url = GetSimilarityUrl(s1, s2)
        val = urllib.urlopen(url).read().strip()
        print val
        try:
            return float(val)
        except ValueError:
            print "oops, that was bad. Retrying ..."

class SimilarityThread(threading.Thread):
    def __init__(self, s1, s2, out):
        threading.Thread.__init__(self)
        self.s1 = s1
        self.s2 = s2
        self.out = out
    def run(self):
        global lk
        result = Similarity(self.s1, self.s2)
        lk.acquire()
        self.out[(self.s1,self.s2)] = result
        lk.release()

def StringIsQuestion(string):
    data = urllib.urlencode({"query":string})
    response = urllib.urlopen("http://nlp.stanford.edu:8080/parser/index.jsp", data).read()
    return (response.find("SBARQ") != -1 or response.find("SINV") != -1)

def EndOfFile(source):
    current = source.stream.wav_reader.tell()
    frames = source.stream.wav_reader.readframes(1)
    source.stream.wav_reader.setpos(current)
    return (len(frames) == 0)

def ParseFile(r, filename):
    with sr.WavFile(filename) as source:
        transcriptions = []
        times = []
        while True:
            (audio, start, stop) = TranscribeListen(source)
            try:
                text = r.recognize(audio)
            except LookupError:
                text = "<Unintelligible>"
            transcriptions.append(text)
            times.appemd((start, stop))
            if EndOfFile(source):
                break
        return (transcriptions, times)

def Echo(r, tts):
    with sr.Microphone() as source:
        audio = r.listen(source)
        text = r.recognize(audio)
        print "You said: " + text
        tts.say("You said: " + text)
        tts.runAndWait()

def GetTextLoop(r, tts, quiet=True):
    t = GetText(r)
    while not t:
        if not quiet:
            tts.say("I couldn't quite catch that. Could you repeat please?")
            tts.runAndWait()
        print "I couldn't quite catch that. Could you repeat?"
        t = GetText(r)
    return t

def GetText(r):
    global x
    with sr.Microphone() as source:
        audio = MyListen(r, source)
        try:
            return r.recognize(audio)
        except LookupError:
            return False

def DumpSoundToFile(source, filename, timeout):
    # record audio data as raw samples
    seconds_per_buffer = (source.CHUNK + 0.0) / source.RATE
    elapsed_time = 0

    with open(filename, "w") as f:
        # store audio input until the phrase starts
        while True:
            elapsed_time += seconds_per_buffer
            if timeout and elapsed_time > timeout: # handle timeout if specified
                break;

            buffer = source.stream.read(source.CHUNK)
            if len(buffer) == 0: 
                break # reached end of the stream

            # check if the audio input has stopped being quiet
            energy = audioop.rms(buffer, source.SAMPLE_WIDTH) # energy of the audio signal
            f.write("%f\n" % energy)

def DetermineEnergyLevelBaseline(source, timeout):
    # record audio data as raw samples
    seconds_per_buffer = (source.CHUNK + 0.0) / source.RATE
    elapsed_time = 0

    energyTotal = 0
    energyCounts = 0

    # store audio input until the phrase starts
    while True:
        elapsed_time += seconds_per_buffer
        if timeout and elapsed_time > timeout: # handle timeout if specified
            break;

        buffer = source.stream.read(source.CHUNK)
        if len(buffer) == 0: 
            break # reached end of the stream

        # check if the audio input has stopped being quiet
        energy = audioop.rms(buffer, source.SAMPLE_WIDTH) # energy of the audio signal
        energyTotal += energy
        energyCounts += 1
    return energyTotal / energyCounts

def When(source, r):
    seconds_per_buffer = (source.CHUNK + 0.0) / source.RATE
    pause_buffer_count = int(math.ceil(r.pause_threshold / seconds_per_buffer))
    
    # record audio data as raw samples
    seconds_per_buffer = (source.CHUNK + 0.0) / source.RATE

    # store audio input until the phrase starts
    while True:
        buffer = source.stream.read(source.CHUNK)
        if len(buffer) == 0: 
            break # reached end of the stream

        # check if the audio input has stopped being quiet
        energy = audioop.rms(buffer, source.SAMPLE_WIDTH) # energy of the audio signal
        if energy > r.energy_threshold:
            print "Starting record..."
            pause_count = 0
            while pause_count <= pause_buffer_count:
                buffer = source.stream.read(source.CHUNK)
                if len(buffer) == 0:
                    break
                energy = audioop.rms(buffer, source.SAMPLE_WIDTH)
                if energy > r.energy_threshold:
                    pause_count = 0
                else:
                    pause_count += 1
            print "Ending record..."
                
    return energyTotal / energyCounts

def AttachmentStatusText(status):
    return skype.Convert.AttachmentStatusToText(status)

def CallStatusText(status):
    return skype.Convert.CallStatusToText(status)

# This handler is fired when status of Call object has changed
def OnCall(call, status):
    global CallStatus
    CallStatus = status
    print 'Call status: ' + CallStatusText(status)

# This handler is fired when Skype attatchment status changes
def OnAttach(status):
    print 'API attachment status: ' + AttachmentStatusText(status)

skype = Skype4Py.Skype()

ser = serial.Serial("COM11", 115200) if blue else None

def main():
    global skype
    global ser

    raw_input()
    
    # Creating Skype object and assigning event handlers..
    skype.OnAttachmentStatus = OnAttach
    skype.OnCallStatus = OnCall

    # Initializing tts
    print 'Initializing text-to-speech..'
    TtsEngine = pyttsx.init()
    TtsEngine.setProperty("rate", 130)

    # Starting Skype if it's not running already..
    if not skype.Client.IsRunning:
        print 'Starting Skype..'
        skype.Client.Start()

    # Attatching to Skype..
    print 'Connecting to Skype..'
    skype.Attach()

    # Measuing noise levels..
    print 'Measuring background noise level..'
    SpeechToText = sr.Recognizer()
    with sr.Microphone() as source:
        baseline = DetermineEnergyLevelBaseline(source, 5)
        print("Energy baseline: %d" % baseline)
        print("Setting energy threshold to 5*baseline, or %d" % (5*baseline))
        SpeechToText.energy_threshold = 5*baseline

    # Waiting for a question..
    print 'Waiting for a question..'
    #Echo(SpeechToText, TtsEngine)

    #with sr.Microphone() as source:
    #    When(source, SpeechToText) 

    db = MakeDB()
    AnswerQuestions(SpeechToText, TtsEngine, db)

    call = skype.PlaceCall(Handle)
    print ' recording ' + OutFile
    call.OutputDevice( Skype4Py.callIoDeviceTypeFile, OutFile )

    def flag():
        return (CallStatus in CallIsFinished)

    #with sr.Microphone() as source:
    #    audio = ListenUntilFlag(SpeechToTest, source, flag)

    # Loop until CallStatus gets one of "call terminated" values in OnCall handler
    while not flag():
        time.sleep(0.1)

    TtsEngine.say('Skype call over')
    TtsEngine.runAndWait()
    
    #t = GetTextLoop(SpeechToText, TtsEngine)
    #print "You said: " + t
    #TtsEngine.say('You said: ' + t)

    #a = GetAnswer(t)

##    if a:
##        #TtsEngine.say(a)
##        #TtsEngine.runAndWait()
##        print "Answer: " + a
##    else:        
##        #TtsEngine.say('I don\'t know the answer to your question. Hold on while I call for help.')
##        #TtsEngine.runAndWait()
##        print "I don't know the answer"
##
##        call = skype.PlaceCall(Handle)
##        print ' recording ' + OutFile
##        call.OutputDevice( Skype4Py.callIoDeviceTypeFile, OutFile )
##
##        # Loop until CallStatus gets one of "call terminated" values in OnCall handler
##        while not CallStatus in CallIsFinished:
##            time.sleep(0.1)
##
##        TtsEngine.say('Skype call over')
##        TtsEngine.runAndWait()

if __name__ == "__main__":
    main()
    ser.close()
    #with sr.Microphone() as source:
    #    DumpSoundToFile(source, "C:\\Users\\Sandro\\Documents\\UofT\\2014-2015\\GYBO\\soundDump.txt", 30)
    #pass
    

