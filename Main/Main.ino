// http://bildr.org/2012/04/tb6612fng-arduino/
//motor A connected between A01 and A02
//motor B connected between B01 and B02

#include <AltSoftSerial.h>

#define STR_LEN 50
#define UnoSerialRx 8 // These are determined by the library and are here for reference
#define UnoSerialTx 9

AltSoftSerial UnoSerial;// Software implementation of serial

int STBY = 10; //standby

//Motor A
const int PWMA = 3; //Speed control 
const int AIN1 = 6; //Direction
const int AIN2 = 7; //Direction

//Motor B
const int PWMB = 5; //Speed control
const int BIN1 = 11; //Direction
const int BIN2 = 12; //Direction

int soundDetectorPin = A0;
boolean searchingForSpeaker = false;

char globalRawString[STR_LEN] = {0};
int rawIndex = 0;

#define IIRINIT 1.0;

float IIR = IIRINIT;


void setup()
{
  Serial.begin(115200);
  Serial.println("Start init");
  pinMode(STBY, OUTPUT);

  pinMode(PWMA, OUTPUT);
  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);

  pinMode(PWMB, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
  
  // Configure bluetooth
  UnoSerial.begin(115200);
  delay(1000);
  // Enter command mode. This allows for configuration of the module without transmitting any data.
  UnoSerial.print("$$$");
  delay(100);
  // Enter slave mode. This configures the module to listen for new connections
  UnoSerial.println("SM,0");
  delay(100);   
  // Exit command mode.
  UnoSerial.println("---");
  
  Serial.println("End init");
}

void loop(void){

 
  char raw_string[100] = {0}; 
  
  if(Serial.available()){
    
      Serial.readBytesUntil(';', raw_string, 100);
      parseAndWriteRawString(raw_string);
   }

    // Special serial reading from bluetooth
    while(UnoSerial.available() > 0){
      
      char val = UnoSerial.read();
      if(val != ';' && val >= 0 && val < 128){
        globalRawString[rawIndex] = val;
        rawIndex ++;
      }else if (val == ';'){
        Serial.print("Terminated string");
         /// Clear and print
         //globalRawString[rawIndex] = ';';
        rawIndex = 0;
        Serial.println(globalRawString);
        // parseAndWriteRawString(globalRawString);
        startSearchingForSpeaker();
        int i = 0;
        for(i = 0; i < STR_LEN; i++){
          globalRawString[i] = 0;
        }
        

         
      }
  
    }
    
    performSpeakerLocationCheck();
    UnoSerial.println("Hello Daneil!");
  delay(50);
}

void parseAndWriteRawString(char* raw_string){// dont include semicolon

      char *command = strtok(raw_string, " ");
      char * mag = strtok(NULL, " ");
      int direction = 0;    
      int magnitude = atoi(mag);  // 16 bits signed
      int motor = -1;
      
      magnitude < 0 ? direction = 0: direction = 1;
      
      Serial.println(raw_string);
      Serial.println(magnitude);

      switch(command[0]){
        case 'L':
          Serial.println("Activate left motor ");
          motor = 0;
          break;
        case 'R':
          Serial.println("Activate right motor");            
          motor = 1;
          break;  
        default:
          stop();      
       }
       
       if((magnitude < 255 && magnitude > -255) && motor != -1){
         Serial.println("Write to motor!");
         move(motor, abs(magnitude), direction);  
       }     
       else{
         Serial.println("Stop hammertime!");
         stop();
       }

}
void move(int motor, int speed, int direction){
//Move specific motor at speed and direction
//motor: 0 for B 1 for A
//speed: 0 is off, and 255 is full speed
//direction: 0 clockwise, 1 counter-clockwise

  digitalWrite(STBY, HIGH); //disable standby

  boolean inPin1 = LOW;
  boolean inPin2 = HIGH;

  if(direction == 1){
    inPin1 = HIGH;
    inPin2 = LOW;
  }

  if(motor == 1){
    digitalWrite(AIN1, inPin1);
    digitalWrite(AIN2, inPin2);
    analogWrite(PWMA, speed);
  }else{
    digitalWrite(BIN1, inPin1);
    digitalWrite(BIN2, inPin2);
    analogWrite(PWMB, speed);
  }
}

void stop(){
//enable standby  
  digitalWrite(STBY, LOW); 
  
}


void startSearchingForSpeaker() {
  move(0, 230, 0);
  move(1, 230, 0);
  searchingForSpeaker = true;
}


void performSpeakerLocationCheck(){
  int distanceValue = analogRead(soundDetectorPin);
  
  if (searchingForSpeaker == true) {
    
//    IIR = IIR * 0.95 + (float) (distanceValue) * 0.05;
 //   Serial.println(IIR);
    if (distanceValue > 60) {
      // We found a person - yay
      //move(0, 0, 0);
      //move(1, 0, 0); 
      stop();
      Serial.println("Guess we stopped");
      searchingForSpeaker = false;
   //   IIR = IIRINIT;
    }
    //\delay(100);
  }
  Serial.println(distanceValue);
}

