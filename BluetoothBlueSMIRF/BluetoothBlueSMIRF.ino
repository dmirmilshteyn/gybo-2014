#include <AltSoftSerial.h>

#define UNO_BUF_LEN 64
#define STR_LEN 50
#define UnoSerialRx 8 // These are determined by the library and are here for reference
#define UnoSerialTx 9

AltSoftSerial UnoSerial;

void setup()
{
  // Open a serial connection with a baud rate of 115200.
  // This is the default for this bluetooth module.
  Serial.begin(115200);
  UnoSerial.begin(115200);
  
  // Wait a second to let the module load.
  delay(1000);
  // Enter command mode.
  // This allows for configuration of the module
  // without transmitting any data.
  Serial.print("$$$");
    UnoSerial.print("$$$");
  // Allow the command to be processed
  delay(100);
  // Enter slave mode.
  // This configures the module to listen for new connections
  Serial.println("SM,0");
  UnoSerial.println("SM,0");
  // Allow the command to be processed
  delay(100);
   
  // Exit command mode.
  // All data sent over the serial port will now be transmitted.
  Serial.println("---"); 
  UnoSerial.println("---");
  
}


char rawString[STR_LEN] = {0};
int rawIndex = 0;

void loop()
{
  // Write a "Hello" message to the serial
  //Serial.println("Hello!");
  UnoSerial.println("Hello Daneil!");
    
  while(UnoSerial.available() > 0){
    
    char val = UnoSerial.read();
    if(val != ';' && val >= 0 && val < 128){
      rawString[rawIndex] = val;
      rawIndex ++;
    }else if (val == ';'){
       /// Clear and print
       rawString[rawIndex] = ';';
      rawIndex = 0;
      Serial.println(rawString);
      int i = 0;
      for(i = 0; i < STR_LEN; i++){
        rawString[i] = 0;
      }
       
    }

  }
  
  // Wait a second before sending the next message
  delay(1000);
}
