import sqlite3
import QADB

qadb = QADB.QADB()
qadb.open("qadbtest.db")
#qadb.createSchema()

answerId = qadb.saveOrSelectAnswer("Test Answer!!!")
print(answerId)
questionId = qadb.saveQuestion("Why is the sky blue?", answerId)
print("Question ID: " + str(questionId))

result = qadb.getStoredAnswer("Why is the sky blue?")
if (result == None):
    print("Guess there wasn't any saved answer for this question...")
else:
    print("The answer is: " + result)

print("All questions:")
for question in qadb.getAllStoredQuestions():
    print(question)

qadb.commit()
qadb.close()